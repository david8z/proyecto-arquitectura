#!/bin/sh

cd /backend/panel_administracion

# collect static files
python manage.py collectstatic --noinput


python manage.py makemigrations

python manage.py migrate --noinput

exec "$@"