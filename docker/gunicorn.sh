#!/bin/sh

# set -o errexit
# set -o pipefail
# set -o nounset

# run gunicorn
gunicorn -b 0.0.0.0:8000 panel_administracion.wsgi --workers ${GUNICORN_WORKERS} --timeout ${GUNICORN_TIMEOUT} $*