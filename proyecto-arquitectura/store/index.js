export const state = () => ({
  preguntas: [
    { contestada: false, texto_escrito: '' },
    { contestada: false, texto_escrito: '' },
    { contestada: false, opcion_seleccionada: [], texto_escrito: '' },
    { contestada: false, opcion_seleccionada: [], texto_escrito: '' }
  ],
  numero_diapositiva: 0,
  adelante: true,
  total_diapositivas: 0
})

export const mutations = {
  numero_diapositiva (state, newValue) {
    if (newValue < state.total_diapositivas && newValue >= 0) {
      state.numero_diapositiva = newValue
    }
  },
  adelante (state, newValue) {
    state.adelante = newValue
  },
  preguntas (state, newValue) {
    state.preguntas[newValue[0]] = newValue[1]
  },
  preguntas_inicio (state, newValue) {
    state.preguntas = newValue.data
  },
  total_diapositivas (state, newValue) {
    state.total_diapositivas = newValue
  }
}

export const actions = {
  async nuxtServerInit (vuexContext, context) {
    vuexContext.commit('preguntas_inicio', (await this.$axios.get('/api/preguntas/')))
  }
}

export const getters = {
  preguntas: state => state.preguntas,
  numero_diapositiva: state => state.numero_diapositiva,
  adelante: state => state.adelante,
  total_diapositivas: state => state.total_diapositivas
}
