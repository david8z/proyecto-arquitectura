from rest_framework import viewsets
from rest_framework.response import Response
from django.db.models import Prefetch


from diapositivas.models import DiapositivaModel
from diapositivas.serializers import DiapositivasSerializer


class DiapositivasViewSet(viewsets.ModelViewSet):
    """
    API endpoint que permite obtener los 4 PostsDestacados.
    """
    serializer_class = DiapositivasSerializer
    http_method_names = ['get']

    def get_queryset(self):
        etilos_pregunta = ['pregunta-escribir', 'pregunta-escribir-seleccionar', 'pregunta-seleccionar', 'pregunta-seleccionar-imagen']
        diapositivas_queryset = DiapositivaModel.objects.order_by('numero_diapositiva').prefetch_related('diapositiva_opcion_texto').prefetch_related('diapositiva_opcion_texto_imagen')
        preguntas_queryset = diapositivas_queryset.filter(estilo__in=etilos_pregunta)
        for index, pregunta in enumerate(preguntas_queryset):
            diapositivas_queryset.filter(id=pregunta.id).update(numero_pregunta=index+1)
        
        return diapositivas_queryset

    def list(self, request):
        queryset = self.get_queryset()
        serializer = DiapositivasSerializer(queryset, many=True)
        return Response(serializer.data)

class PreguntasViewSet(viewsets.ModelViewSet):
    """
    API endpoint que permite obtener los 4 PostsDestacados.
    """
    http_method_names = ['get']

    def get_queryset(self):
        etilos_pregunta = ['pregunta-escribir', 'pregunta-escribir-seleccionar', 'pregunta-seleccionar', 'pregunta-seleccionar-imagen']
        diapositivas_queryset = DiapositivaModel.objects.order_by('numero_diapositiva').prefetch_related('diapositiva_opcion_texto').prefetch_related('diapositiva_opcion_texto_imagen')
        preguntas_queryset = diapositivas_queryset.filter(estilo__in=etilos_pregunta)
        
        return preguntas_queryset

    def list(self, request):
        queryset = self.get_queryset()
        response = []
        for pregunta in queryset:
            if pregunta.estilo in ['pregunta-escribir', 'pregunta-escribir-seleccionar']:
                response.append({'contestada': False, 'texto_escrito': ''})
            else:
                response.append({'contestada': False, 'opcion_seleccionada': [], 'texto_escrito': ''})

        return Response(response)