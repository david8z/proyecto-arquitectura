from django.apps import AppConfig


class DiapositivasConfig(AppConfig):
    name = 'diapositivas'
    verbose_name = "Diapositivas Formulario"
