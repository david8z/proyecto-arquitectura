from django.contrib import admin
from adminsortable2.admin import SortableAdminMixin
from diapositivas.models import DiapositivaModel, OpcionTexto, OpcionImagen
from diapositivas.forms import DiapositivaModelForm
from django.utils.html import mark_safe

from django.contrib.auth.models import Group, User

admin.site.unregister(Group)
admin.site.unregister(User)


class OpcionTextoInline(admin.TabularInline):
    fields = ('texto',)
    classes = ('opcion_texto',)
    model = OpcionTexto
    extra = 0

class OpcionImagenInline(admin.TabularInline):
    fields = ('texto', 'imagen',)
    classes = ('opcion_imagen',)
    model = OpcionImagen
    extra = 0

class DiapositivaModelAdmin(SortableAdminMixin, admin.ModelAdmin):
    list_display = [ 'estilo', 'texto_principal_display',]

    def texto_principal_display(self, obj):
        # return HTML link that will not be escaped
        return mark_safe(
            '<div style="max-height: 20px;overflow:hidden;">%s</div>' % obj.texto_principal
        )
    texto_principal_display.short_description = 'texto principal'

    fieldsets = (
        ('ESTILO', {
            'fields': ('estilo',),
            'classes': ('predefined',)
        }),
        ('TEXTOS', {
            'fields': ('texto_principal', 'texto_secundario', 'texto_boton',),
            'classes': ('texto',)
        }),
        ('IMAGEN', {
            'fields': ('imagen_texto_imagen',),
            'classes': ('imagen',)
        }),
        ('ESCRICBIR TEXTO', {
            'fields': ('texto_label',),
            'classes': ('escribir',)
        }),
        ('SELECCIONAR OPCIONES', {
            'fields': ('opciones_validas', 'otra_opcion',),
            'classes': ('seleccionar',)
        })
    )

    form = DiapositivaModelForm
    inlines = [OpcionTextoInline, OpcionImagenInline]
    class Media:
        js = (
             '//ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js',
            'diapositiva/js/base.js',
            )

admin.site.register(DiapositivaModel, DiapositivaModelAdmin)