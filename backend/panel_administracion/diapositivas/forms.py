from django import forms
from diapositivas.models import DiapositivaModel

class DiapositivaModelForm(forms.ModelForm):

    class Meta:
        model = DiapositivaModel
        fields = ('estilo',)
        widgets = {
            'estilo': forms.Select(choices=DiapositivaModel.CHOICES)
        }