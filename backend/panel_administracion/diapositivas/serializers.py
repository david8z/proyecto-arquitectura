"""DOC STRING"""
import django.core.checks


from rest_framework import serializers
from diapositivas.models import DiapositivaModel, OpcionImagen, OpcionTexto


###########################
##DIAPOSITIVAS SERIALIZER##
###########################
@django.core.checks.register('rest_framework.serializers')
class OpcionImagenSerializer(serializers.ModelSerializer):
    """DOCSTRING"""

    class Meta:
        model = OpcionImagen
        fields = ['texto','value', 'imagen']

@django.core.checks.register('rest_framework.serializers')
class OpcionTetxoSerializer(serializers.ModelSerializer):
    """DOCSTRING"""

    class Meta:
        model = OpcionTexto
        fields = ['texto','value']

@django.core.checks.register('rest_framework.serializers')
class DiapositivasSerializer(serializers.ModelSerializer):
    """DOCSTRING"""
    diapositiva_opcion_texto = OpcionTetxoSerializer(many=True)
    diapositiva_opcion_texto_imagen = OpcionImagenSerializer(many=True)
    class Meta:
        model = DiapositivaModel
        fields = ['estilo', 'numero_pregunta', 'texto_principal', 'texto_secundario', 'texto_label', 'texto_boton', 'imagen_texto_imagen', 'otra_opcion', 'opciones_validas', 'diapositiva_opcion_texto', 'diapositiva_opcion_texto_imagen']
