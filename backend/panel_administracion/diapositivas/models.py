from django.db import models
from ckeditor.fields import RichTextField

from django.db.models.signals import pre_save

from django.utils.text import slugify


class DiapositivaModel(models.Model):

    CHOICES = (
        ('texto', 'Texto'),
        ('texto-imagen', 'Texto con Imagen'),
        ('cita', 'Cita'),
        ('pregunta-escribir', 'Pregunta Escribir'),
        ('pregunta-escribir-seleccionar', 'Pregunta Escribir o Seleccionar'),
        ('pregunta-seleccionar', 'Pregunta Seleccionar'),
        ('pregunta-seleccionar-imagen', 'Pregunta Seleccionar con Imagen')
    )

    estilo = models.CharField(max_length=30, help_text="Selecciona el estilo que deseas utilizar", choices=CHOICES)
    texto_principal = RichTextField(help_text="Texto principal que se mostrará en la diapositiva. CAMPO OBLIGATORIO")
    texto_secundario = RichTextField(blank=True, null=True, help_text="Texto que aparece debajo del texto principal en un tono más clasro. CAMPO OPCIONAL")
    texto_label = models.CharField(max_length=100, help_text="Texto que aparece en el campo de texto antes de escribir.", blank=True, null=True)
    texto_boton = models.CharField(max_length=30, help_text="Texto que aparece en el botón.")
    imagen_texto_imagen = models.ImageField(upload_to='imagen_texto_imagen', verbose_name='Imagen a mostrar', null=True, blank=True)
    otra_opcion = models.CharField(max_length=100, verbose_name='Texto botón otra opción', help_text="Texto que aparece en el botón donde puedes seleccionar otra opción.", blank=True, null=True)
    opciones_validas = models.IntegerField(default=1, blank=True, null=True, help_text="Número de opciones que el usuaio puede seleccionar en pregunta escribir -1 en caso de que pueda seleccionar todas las que quiera.")


    numero_diapositiva = models.PositiveIntegerField(default=0, blank=False, null=False, verbose_name="Orden")
    numero_pregunta = models.PositiveIntegerField(blank=True, null=True,)
    def __str__(self):
        return self.estilo
    
    class Meta(object):
        verbose_name = "Diapositiva"
        verbose_name_plural = "Diapositivas"
        ordering = ['numero_diapositiva']


class OpcionTexto(models.Model):
    texto = models.CharField(max_length=100, help_text="Texto que representa la opción seleccionada")
    value = models.SlugField(blank=True, null=True)
    diapositiva = models.ForeignKey(DiapositivaModel, on_delete=models.CASCADE, related_name='diapositiva_opcion_texto')

    def __str__(self):
        return ''

class OpcionImagen(models.Model):
    texto = models.CharField(max_length=100, help_text="Texto que representa la opción seleccionada")
    imagen = models.ImageField(upload_to='imagen_opcion_imagen', help_text="Se recomienda que las dimensiones de las imágenes seán 1:1")
    value = models.SlugField(blank=True, null=True)
    diapositiva = models.ForeignKey(DiapositivaModel, on_delete=models.CASCADE, related_name='diapositiva_opcion_texto_imagen')

    def __str__(self):
        return ''


def opcion_texto_pre_save_receiver(sender, instance, *args, **kwargs):
    """
    Metodo encargado de generar el slug field al crear el texto seo.
    """
    instance.value = slugify(instance.texto)


def opcion_imagen_pre_save_receiver(sender, instance, *args, **kwargs):
    """
    Metodo encargado de generar el slug field al crear el texto seo.
    """
    instance.value = slugify(instance.texto)

pre_save.connect(opcion_texto_pre_save_receiver, sender=OpcionTexto)
pre_save.connect(opcion_imagen_pre_save_receiver, sender=OpcionImagen)
