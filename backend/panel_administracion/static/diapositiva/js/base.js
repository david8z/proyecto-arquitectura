(function($) {
    $(function() {
        var selectField = $('#id_estilo'),
            texto = $('.texto'),
            imagen = $('.imagen'),
            escribir = $('.escribir'),
            seleccionar = $('.seleccionar'),
            opcion_texto = $('.opcion_texto'),
            opcion_imagen = $('.opcion_imagen');

        function toggleVerified(value) {
            if(value){
                texto.show();
            } else {
                texto.hide();
            }
            if (value === 'pregunta-escribir' || value === 'pregunta-escribir-seleccionar') {
                escribir.show();
            } else {
                escribir.hide();
            }
            if (value === 'pregunta-seleccionar' || value === 'pregunta-seleccionar-imagen') {
                seleccionar.show();
            } else {
                seleccionar.hide();
            }
            if (value === 'texto-imagen') {
                imagen.show();
            } else {
                imagen.hide();
            }
            if (value === 'pregunta-seleccionar' || value === 'pregunta-escribir-seleccionar') {
                opcion_texto.show();
            } else {
                opcion_texto.hide();
            }
            if (value === 'pregunta-seleccionar-imagen') {
                opcion_imagen.show();
            } else {
                opcion_imagen.hide();
            }
        }
        
        
        // show/hide on load based on pervious value of selectField
        toggleVerified(selectField.val());

        // show/hide on change
        selectField.change(function() {
            toggleVerified($(this).val());
        });
    });
})(django.jQuery);